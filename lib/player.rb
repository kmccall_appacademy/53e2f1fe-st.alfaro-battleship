class HumanPlayer
  def initialize(name = "John Doe")
    @name = name
  end

  def get_play
    puts "Your play"
    gets.chomp.split(",").map(&:to_i)
  end

end
