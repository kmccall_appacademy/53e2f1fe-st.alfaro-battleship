require 'byebug'
class Board
  DEFAULT_GRID_SIZE = 10
  attr_reader :grid
  def initialize(grid = Board.default_grid)
    @grid = grid
    @boats = get_n_boats
  end

  def self.default_grid
    Array.new(DEFAULT_GRID_SIZE) { Array.new(DEFAULT_GRID_SIZE) }
  end

  def count
    @boats
  end

  def empty?(pos = nil)
    if pos.nil?
      count == 0
    else
      !self[pos]
    end
  end

  def full?
    count == grid.flatten.length
  end

  def won?
    segment_proc = Proc.new { |val| val == :s }
    segments = reduce_grid(segment_proc)
    segments == 0 ? true : false
  end

  def place_random_ship
    raise "Board is full, can not place more ships" if full?
    random = Random.new
    row = nil
    col = nil
    loop do
      row = random.rand(grid.length)
      col = random.rand(grid[row].length)
      break if empty?([row, col])
    end
    place_ship([row, col])
  end

  def place_ship(pos)
    self[pos] = :s
    @boats += 1
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, status)
    row, col = pos
    @grid[row][col] = status
  end

  private

  def get_n_boats
    boat_proc = Proc.new { |val| val }
    reduce_grid(boat_proc)
  end

  def reduce_grid(prc)
    grid.reduce(0) do |tracker, row_val|
      tracker + row_val.reduce(0) do |inner_tracker, val|
        inner_tracker += 1 if prc.call(val)
        inner_tracker
      end
    end
  end

end

class BoardCell

end
